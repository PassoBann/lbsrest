<?php
require_once("../vendor/autoload.php");
header('Content-Type: application/json');

// Token : page arche du module : ressources php : liste de librairies de qualité : randomLib

use Illuminate\Database\Capsule\Manager as Capsule;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use lbs\model\Categorie as Categorie;
use lbs\internals\controllers\PrivateCatalogueController as PvCatalogueController;

$capsule = new Capsule;

$capsule->addConnection(parse_ini_file('../../LBSconfig/config.ini'),"default");
$capsule->setAsGlobal();
$capsule->bootEloquent();

$configuration = [
 'settings' => [
 'displayErrorDetails' => true,
'production' => false ]
];

$c = new \Slim\Container($configuration);

/*END INIT*/


$app = new \Slim\App($c);

//Liste complète des commandes
$app->get('/commandes/', function (Request $request, Response $response, $args) {
    return (new PvCatalogueController($this))->listCommandes($request, $response, $args);
})->setName('commandes');

//Liste des commandes par pages
$app->get('/commandes/{offset}/{limit}/', function (Request $request, Response $response, $args) {
    return (new PvCatalogueController($this))->listCommandesByPage($request, $response, $args);
})->setName('commandesbyPage');

//Liste des commandes par statut
$app->get('/commandes/{status}/', function (Request $request, Response $response, $args) {
    return (new PvCatalogueController($this))->listCommandesByStatus($request, $response, $args);
})->setName('commandesbyStatus');

//Détails d'une commande
$app->get('/commande/{id}/', function (Request $request, Response $response, $args) {
    return (new PvCatalogueController($this))->seeCommande($request, $response, $args);
})->setName('commande');

//Changement de statut d'une commande
$app->put('/commande/{id}/status/', function (Request $request, Response $response, $args) {
    return (new PvCatalogueController($this))->changeCommandeStatus($request, $response, $args);
})->setName('changeStatus');

$app->run();
