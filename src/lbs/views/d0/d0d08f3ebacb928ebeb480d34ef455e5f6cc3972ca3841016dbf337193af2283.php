<?php

/* authSuccess.html.twig */
class __TwigTemplate_712c68462c7a81cbda8b30c729b17e9412eaf410b495934146897cb26cd1b7ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1>";
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</h1>
<p>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</p>
<button><a href=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["addIngredient"]) ? $context["addIngredient"] : null), "html", null, true);
        echo "\">Ajouter un ingrédient</a></button>
<button><a href=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["tdb"]) ? $context["tdb"] : null), "html", null, true);
        echo "\">Tableau de bord</a></button>
<button><a href=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["getIngredients"]) ? $context["getIngredients"] : null), "html", null, true);
        echo "\">Lister les ingrédients</a></button>";
    }

    public function getTemplateName()
    {
        return "authSuccess.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 5,  32 => 4,  28 => 3,  24 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>{{title}}</h1>
<p>{{message}}</p>
<button><a href=\"{{addIngredient}}\">Ajouter un ingrédient</a></button>
<button><a href=\"{{tdb}}\">Tableau de bord</a></button>
<button><a href=\"{{getIngredients}}\">Lister les ingrédients</a></button>", "authSuccess.html.twig", "/var/www/REST/lbsrest/src/lbs/views/authSuccess.html.twig");
    }
}
