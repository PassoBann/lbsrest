<?php
namespace lbs\api\controllers;
use lbs\model\Ingredient as Ingredient;


class IngredientController
{
	protected $root;
	public function __construct($root)
	{
		$this->root = $root;
	}

	public function getIngredient($request, $response, $args) {

	    $id = $args['id'];

	    $response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

	    if (Ingredient::find($id) != NULL) {
	    	$response = $response->withStatus(200);

	    	$data = Ingredient::find($id);
	    	$cat = Ingredient::find($id)->is_in_categorie()
	        	->where('id','=',$data->cat_id)
	        	->get();
	        //$category = array(json_decode(Ingredient::find($id)));
	        $array =
	        ['ingredient' =>
	        	[
		        	'id' => $data->id,
		        	'nom' => $data->nom,
		        	'description' => $data->description,
		        	'fournisseur' => $data->fournisseur,
		        	'img' => $data->img,
		        	'categorie' => $cat

	        	]
	        ];
	        $links =
	        ['links' =>
	        	[
	        	'categorie' =>
	        		['href' => $this->root['router']->pathFor('category',
	        				['id'=>$data->cat_id
	        			])
	        		]
	        	]
	        ];

	        $combined = array_merge($array,$links);
	    	$response->getBody()->write(json_encode($combined));

	    }

	    else {
	        $response = $response->withStatus(404);
	        $response->getBody()->write(
	        	json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('category',['id'=>$id])]));
	    }

	    return $response;

	}

	public function getIngredientList($request, $response) {
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

			$cat = Ingredient::select("*");
	        $ingredients_list = $cat->is_in_categorie()->get();


			$response->getBody()->write(json_encode($ingredients_list));
	}

	public function postIngredient($request, $response) {

		
			$files = $request->getUploadedFiles();
			$data = $request->getParsedBody();


		    if (Ingredient::find($data['categorie']) != NULL && isset($data['categorie']) && isset($data['ingredient']) && isset($data['description']) && isset($data['fournisseur'])) {

				//Filtrage des données
		    	$noming = filter_var($data['ingredient'], FILTER_SANITIZE_STRING);
		    	$fournisseur = filter_var($data['fournisseur'], FILTER_SANITIZE_STRING);
		    	$description = filter_var($data['description'], FILTER_SANITIZE_STRING);
				$categorie = filter_var($data['categorie'], FILTER_SANITIZE_NUMBER_INT);

				//Traitement de l'image
				$random = md5(uniqid(rand(),true));
				$nomimg = $random.'.jpg';
				$nom = "../manager/assets/".$nomimg;
				$resultat = move_uploaded_file($files['image']->file, $nom);

				//Définition du nouvel ingrédient 
				$ingredient = new Ingredient;
				$ingredient->nom = $noming;
				$ingredient->cat_id = $categorie;
				$ingredient->description = $description;
				$ingredient->fournisseur = $fournisseur;
				$ingredient->img = $nomimg;

				if($ingredient->save()) {

					$response = $response->withStatus(201);

					$title = "Succès :";
					$message = "L'ingrédient ".$noming." a bien été ajouté.";

					return $this->root['view']->render($response, 'message.html.twig',
					['title' => $title,
					'message' => $message]);

				}
				else {

					$title = "ERREUR";
					$error = "La catégorie n'existe pas ou vous n'avez pas renseigné toutes les informations.";

					return $this->root['view']->render($response, 'message.html.twig',
					['message' => $error,
					'title' => $title]);
				
				}

			}
			else{
				$error = "La catégorie n'existe pas ou vous n'avez pas renseigné toutes les informations.";
				return $this->root['view']->render($response, 'errors.html.twig',
				['error' => $error]);
			}

	}

	public function deleteIngredients($request, $response){

		$ingredients = $_POST['ingredients'];
		$title = "ERREUR";

		foreach ($ingredients as $id) {

			//Vérification de l'existance de la ressource
			try {
				$i = Ingredient::findOrFail($id);
			} Catch (ModelNotFoundException $e) {
				$message = $id . " not found";
				return $this->root['view']->render($response, 'message.html.twig', ['message' => $message, 'title' => $title]);
			}

			//Vérification de la date de suppression
			if ($i->dateHeureSuppression != NULL) {
				$message = "Ingredient " . $id ." already deleted : ". Ingredient::find($id)->dateHeureSuppression;
				return $this->root['view']->render($response, 'message.html.twig', ['message' => $message, 'title' => $title]);
			}

			date_default_timezone_set('Europe/Paris');
			$datetime = new \DateTime();
			$i->dateHeureSuppression = $datetime->format("Y-m-d H:i:s");

			//Mise à jour dans la base de donnée
			try {
				$i->save();
			} Catch (\Exception $e) {
				$message = "Query error : " . $e->getMessage();
				return $this->root['view']->render($response, 'message.html.twig', ['message' => $message, 'title' => $title]);
			}

		}// end foreach

		//Affichage
		$title = "SUCCÉS";
		$message = "Ingredient(s) deleted";
	 	return $this->root['view']->render($response, 'message.html.twig', ['message' => $message, 'title' => $title]);

	}

}
