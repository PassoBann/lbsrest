<?php
namespace lbs\model;

class Ingredient extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'ingredient';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
    public function is_in_categorie () {
		return $this->belongsTo('\lbs\model\Categorie','cat_id');
	}
}