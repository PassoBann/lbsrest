<?php
namespace lbs\api\controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use lbs\model\Ingredient as Ingredient;
use lbs\model\Sandwich as Sandwich;
use lbs\model\Commande as Commande;
use lbs\model\Taille as Taille;
use lbs\model\TypePain as Type;
use lbs\model\Status as Status;
use Illuminate\Database\Capsule\Manager as DB;


const CREATED 	= 1;
const PAYED 		=	2;
const PENDING 	= 3;
const READY 		= 4;
const DELIVERED = 5;


class SandwichController
{
	protected $root;
	public function __construct($root)
	{
		$this->root = $root;
	}



	public function addSandwich($request, $response, $args) {

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

		$data = $request->getParsedBody();

		//Verification arguments
    	if (isset($data["taille"]) && isset($data['id_type_pain']) && Taille::find($data['taille']) !== null) {

    		//PARAMETRES ENVOYES
	        $taille = filter_var($data['taille'],FILTER_SANITIZE_NUMBER_INT);
	        $type_pain = filter_var($data['id_type_pain'],FILTER_SANITIZE_NUMBER_INT);
	        $token = filter_var($_GET['token'], FILTER_SANITIZE_URL);
	        $id_commande = filter_var($args['id'], FILTER_SANITIZE_NUMBER_INT);

	        $getNbIng = Taille::find($taille);
	        $nbIng = count($data['ingredients']);

	        //Vérification du nombre d'ingrédients
	        if ($nbIng <= $getNbIng->nbingr) {

		        $commande = Commande::find($id_commande);

		        //Vérification token
		        if ($token == $commande->token) {

		        	//Vérification du status
		        	if ($commande->status == 1) {


					    $new_sandwich = new Sandwich;
					    $new_sandwich->id_taille = $taille;
				        $new_sandwich->id_type_pain = $type_pain;
				        $new_sandwich->id_commande = $id_commande;

				        if ($new_sandwich->save()) {

				        	$prixtot = Commande::find($id_commande);

				        	$prix = $getNbIng->prix;
				        	$prixtot->prixtot = $prixtot->prixtot + $prix;
				        	$prixtot->save();



				           	foreach ($data['ingredients'] as $ingredient) {
				           		$ingredient = filter_var($ingredient,FILTER_SANITIZE_NUMBER_INT);
						       	$new_sandwich->ingredients()->attach($ingredient);
						    }

						    $ingList = $new_sandwich->ingredients;


					        $list = [];
					        foreach ($ingList as $value) {
					            array_push($list, [
					   				'id' => $value->id,
			         				'nom' => $value->nom,
		            				'description' => $value->description,
		            				'fournisseur' => $value->fournisseur,
		            				'img' => $value->img,
					            	]
					            );
					        }

				            $array = [
					            'sandwich'=> [
					            	'id' => $new_sandwich->id,
					            	'taille' => Taille::find($new_sandwich->id_taille)->lib,
					            	'type_pain' => Type::find($new_sandwich->id_type_pain)->lib,
					            	'id_commande' => $new_sandwich->id_commande,
					            ],
					            'ingredients' => $list
				            ];

				            $links = [
				            'links' =>
				                [
				                    'self' => $this->root['router']->pathFor('newSandwich',['id'=>$new_sandwich->id_commande]),
				                    'modify' => [
				                    	'href' => $this->root['router']->pathFor('updateSandwich', ['id'=>$new_sandwich->id_commande])],
				                    'delete' => [
				                    	'href' => $this->root['router']->pathFor('deleteSandwich', ['id'=>$new_sandwich->id_commande])]
				                ]
					        ];


					        $combined = array_merge($array,$links);
					        $response->getBody()->write(json_encode($combined));

				            //Ajout dans le header
				            $method = $request->getMethod();
				            $uri = $request->getUri();
				            $response = $response->withHeader('Location:', $this->root['router']->pathFor('newSandwich',
					    			[
					            		'id'=>$new_sandwich->id

					            	]
					           	)
				            );

					        $response = $response->withStatus(201);

					        return $response;
					    }
					    else {
					        $response->getBody()->write("Une erreur est survenue lors de la création du sandwich.");
					    }
					}//Verification status
			    	else {

						$error = [
			    			'error' => [
			    				'message' => 'Your command has bad status',
			    				'href' => $this->root['router']->pathFor('newSandwich',['id'=>$args['id']])
			    			]
			    		];

			    		$method = $request->getMethod();
				        $uri = $request->getUri();
						$response = $response->withStatus(403);
				       	$response->getBody()->write(json_encode($error));

		    			return $response;
			    	}
			    }//Verification token
		    	else {

		    		$error = [
		    			'error' => [
		    				'message' => 'Bad token',
		    				'href' => $this->root['router']->pathFor('newSandwich',['id'=>$args['id']])
		    			]
		    		];

		    		$method = $request->getMethod();
			        $uri = $request->getUri();
					$response = $response->withStatus(403);
			        $response->getBody()->write(json_encode($error));

		    		return $response;


		    	}

		    }//Fin vérification du nombre d'ingrédients

		    else {
		    	$error = [
			    	'error' => [
			    		'message' => 'Your sandwich size is too short to have '.$nbIng.' ingredients',
			    		'href' => $this->root['router']->pathFor('newSandwich',['id'=>$args['id']])
			    	]
			    ];

			    $method = $request->getMethod();
				$uri = $request->getUri();
				$response = $response->withStatus(403);
				$response->getBody()->write(json_encode($error));

		    	return $response;

		    }

	    }//Fin vérification des args

		else {

			$error = [
		    	'error' => [
		    		'message' => 'Element of your sandwich or command is empty',
		    		'href' => $this->root['router']->pathFor('newSandwich',['id'=>$args['id']])
		    	]
		    ];

		    $method = $request->getMethod();
			$uri = $request->getUri();
			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));

	    	return $response;

		}

	return $response;

	}


	public function getSandwich($request, $response, $args) {

		$id = $args['id'];

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

		try {
			$c = Commande::where('id', $id)->with('sandwiches', 'sandwiches.ingredients')->firstOrFail();
		} Catch (ModelNotFoundException $e) {
			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode(["error" => "Ressource not found :".$this->root['router']->pathFor('sandwich',['id'=>$id])]));
			return $response;
		}
		echo json_encode($c); die();

		$ls = $c->has_sandwich;
		foreach ($ls as $s){
			$li = $s->ingredients;
		}

		$array = [
			'sandwich'=> [
				'id' => $c->id,
				'ingredients' => $i
			]
		];

		$links = [
		'links' =>
			[ 'ingredients' =>
				['href' => $this->root['router']->pathFor('sandwich',['id'=>$s->id]) . '/ingredients']
			]
		];

		$combined = array_merge($array,$links);

		$response->getBody()->write(json_encode($combined));

		return $response;

	}

	public function updateSandwich($request, $response, $args) {

		$id = $args['id'];

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

    $data = $request->getParsedBody();

		//Vérification de l'existance de la ressource
		try {
			$s = Sandwich::where('dateHeureSuppression', '=', null)->findOrFail($id);
		} Catch (ModelNotFoundException $e) {
			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode(["error" => "Ressource not found : ".$this->root['router']->pathFor('updateSandwich',['id'=>$id])]));
			return $response;
		}

		//Vérification de l'existance du token
		if (!isset($_GET['token'])){
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Token not found"]));
			return $response;
		}

		//Vérification du token
		$token = filter_var($_GET['token'], FILTER_SANITIZE_URL);
		$c = Commande::find($s->id_commande);

		if ($token != $c->token) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Bad token : ". $token]));
			return $response;
		}

		//Vérification si le sandwich a été supprimé
		//if ($s->dateHeureSuppression != NULL) {
		//	$response = $response->withStatus(403);
		//	$response->getBody()->write(json_encode(["error" => "Sandwich already deleted : ". Sandwich::find($id)->dateHeureSuppression]));
		//	return $response;
		//}

		//Vérification du statut de la commande
		if ($c->status != CREATED) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Command can't be updated with status : ". Status::find($c->status)->lib]));
			return $response;
		}

		//Vérification du nombre d'ingrédients
		if (isset($data['ingredients'])) {
				$nbIng = count($data['ingredients']);
		}

		$t = $s->taille;
		if ($nbIng <= $t->nbingr) {
			$i = [];
			foreach ($data['ingredients'] as $ingredient) {
				$ingredient = filter_var($ingredient,FILTER_SANITIZE_NUMBER_INT);
				if (Ingredient::find($ingredient) != NULL) {
					$i[] = $ingredient;
				} else {
					$response = $response->withStatus(403);
					$response->getBody()->write(json_encode(["error" => "Ingredient identifier doesn't exists : ". $ingredient]));
					return $response;
				}
			}
			$s->ingredients()->sync($i);
		} else {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Ingredient's quantity too high : max is ". $t->nbingr]));
			return $response;
		}

		// Vérification de la mise à jour du type de pain
		if (isset($data['id_type_pain'])){
			$id_type_pain = filter_var($data['id_type_pain'],FILTER_SANITIZE_NUMBER_INT);
			$s->id_type_pain = $id_type_pain;
		}

		//Vérification de l'existance du type de pain
		if(Type::find($s->id_type_pain) == NULL){
				$response = $response->withStatus(403);
				$response->getBody()->write(json_encode(["error" => "Type identifier doesn't exist : ". $s->id_type_pain]));
				return $response;
		}

		// Mise à jour dans la base de données
		try {
			$s->save();
		} Catch (\Exception $e) {
				$response = $response->withStatus(500);
				$response->getBody()->write(json_encode(["error" => "Query error : ". $e->getMessage()]));
				return $response;
		}

		//Affichage
		$ci = $s->ingredients;

		$li = [];
		foreach ($ci as $value) {
				array_push($li, [
					'id' => $value->id,
					'nom' => $value->nom,
					'description' => $value->description,
					'fournisseur' => $value->fournisseur,
					'img' => $value->img,
					]
				);
		}

		$array = [
			'sandwich'=> [
				'id' => $s->id,
				'taille' => Taille::find($s->id_taille)->lib,
				'type_pain' => Type::find($s->id_type_pain)->lib,
				'id_commande' => $s->id_commande,
			],
			'ingredients' => $li
		];

		$links = [
		'links' =>
				[
						'addSandwich' => $this->root['router']->pathFor('newSandwich',['id'=>$s->id_commande]),
						'self' => [
							'href' => $this->root['router']->pathFor('updateSandwich', ['id'=>$id])],
						'deleteSandwich' => [
							'href' => $this->root['router']->pathFor('deleteSandwich', ['id'=>$id])]
				]
		];


		$combined = array_merge($array,$links);
		$response = $response->withStatus(200);
		$response->getBody()->write(json_encode($combined));

		return $response;
	}

	public function deleteSandwich($request, $response, $args){

		$id = $args['id'];

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

		$data = $request->getParsedBody();

		//Vérification de l'existance de la ressource
		try {
			$s = Sandwich::findOrFail($id);
		} Catch (ModelNotFoundException $e) {
			$response = $response->withStatus(404);
			$response->getBody()->write(
			json_encode(["error" => "Ressource not found :".$this->root['router']->pathFor('deleteSandwich',['id'=>$id])]));
			return $response;
		}

		//Vérification de l'existance du token
		if (!isset($_GET['token'])){
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Token not found"]));
			return $response;
		}

		//Vérification du token
		$token = filter_var($_GET['token'], FILTER_SANITIZE_URL);
		$c = Commande::find($s->id_commande);

		if ($token != $c->token) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Bad token : ". $token]));
			return $response;
		}

		//Vérification du statut de la commande
		if ($s->commande->status != CREATED) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Command can't be modified with status : ". Status::find($s->commande->status)->lib]));
			return $response;
		}

		//Vérification de la date de suppression
		if ($s->dateHeureSuppression != NULL) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Sandwich already deleted : ". Sandwich::find($id)->dateHeureSuppression]));
			return $response;
		}

		date_default_timezone_set('Europe/Paris');
		$datetime = new \DateTime();
		$s->dateHeureSuppression = $datetime->format("Y-m-d H:i:s");

		//Mise à jour dans la base de donnée
		try {
			$s->save();
		} Catch (\Exception $e) {
			$response = $response->withStatus(500);
			$response->getBody()->write(json_encode(["error" => "Query error : ". $e->getMessage()]));
			return $response;
		}

		$sandwich = [
			'sandwich'=> [
				'id' => $s->id,
				'taille' => $s->taille->lib,
				'type_pain' => $s->get_type->lib,
				'id_commande' => $s->id_commande,
				'dateHeureSuppression' => $s->dateHeureSuppression
			]
		];

		$links = [
		'links' =>
				[
						'AddSandwich' => $this->root['router']->pathFor('newSandwich',['id'=>$s->id_commande]),
						'modifySandwich' => [
							'href' => $this->root['router']->pathFor('updateSandwich', ['id'=>$id])],
						'self' => [
							'href' => $this->root['router']->pathFor('deleteSandwich', ['id'=>$id])]
				]
		];


		//Affichage
		$response = $response->withStatus(200);
		$combined = array_merge($sandwich,$links);
		$response->getBody()->write(json_encode($combined));

		return $response;
	}

	public function getTailleList($request,$response) {

		$tailles = Taille::select('*')->get();

		$path = $this->root['router']->pathFor('updateTaille');
		return $this->root['view']->render($response, 'tailleList.html.twig',
			['tailles' => $tailles, 'path' => $path]);
	}

	public function updateTaille($request, $response){

		$title = "ERREUR";
		//Vérification de l'existance de la ressource
		try {
			$t = Taille::findOrFail($_POST['id']);
		} Catch (ModelNotFoundException $e) {
			$message = $_POST['id'] . " not found";
			return $this->root['view']->render($response, 'message.html.twig', ['message' => $message, 'title' => $title]);
		}

		//Mise à jour du libellé et du prix
		$lib = filter_var($_POST['lib'],FILTER_SANITIZE_STRING);
		$prix = filter_var($_POST['prix'],FILTER_SANITIZE_NUMBER_INT);
		$t->prix = $prix;
		$t->lib = $lib;

		try {
			$t->save();
		} Catch (\Exception $e) {
			$message = "Query error : " . $e->getMessage();
			return $this->root['view']->render($response, 'message.html.twig', ['message' => $message, 'title' => $title]);
		}

		//Affichage
		$title = "SUCCÈS";
	 	$message = "Taille mise à jour";
	 	return $this->root['view']->render($response, 'message.html.twig', ['message' => $message, 'title' => $title]);

	}

}
