<?php
namespace lbs\model;

class Categorie extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'categorie';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
    public function has_ingredients () {
		return $this->hasMany('lbs\model\Ingredient','cat_id');
	}
}