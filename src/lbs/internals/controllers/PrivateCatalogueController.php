<?php
namespace lbs\internals\controllers;
use lbs\model\Commande as Commande;
use lbs\model\Status as Status;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class PrivateCatalogueController
{
	protected $root;
	public function __construct($root)
	{
		$this->root = $root;
	}

//	Liste de toutes les commandes
	public function listCommandes($request, $response) {
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		try {
			$commandes_count = Commande::count();
		    $commandes_list = Commande::select('id','token','status','dateHeureRetrait','prixTot')->orderBy('dateHeureRetrait')->orderBy('id')->get();
		}
		catch (ModelNotFoundException $e){
			$error = [
				'error' => [
					'message' => 'Ressource not found',
					'href' => $this->root['router']->pathFor('commandes')
				]
			];

			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

	    $collection = [];

		foreach($commandes_list as $cat){
			$tab = $cat->toArray();
			array_push($collection, [
				'commande' => $tab,
				'links' => ['self' =>
					['href' => $this->root['router']->pathFor('commande',['id'=>$tab['id']])]]
					]);

		}

	    $commandes = array(
	        'nb' => $commandes_count,
	        'commandes' => $collection
	    );

	    $response->getBody()->write(json_encode($commandes));
		$response->withStatus(200);
	    return $response;
	}

//	Liste de toutes les commandes
	public function listCommandesByStatus($request, $response, $args) {
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		$status = filter_var($args['status'],FILTER_SANITIZE_STRING);
		try {
			$status_count = Status::count();
		}
		catch (ModelNotFoundException $e){
			$error = [
				'error' => [
					'message' => 'Ressource not found',
					'href' => $this->root['router']->pathFor('commandesbyStatus',['status'=>$status])
				]
			];

			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

		if ($status < 1 || $status > $status_count) {
			$error = [
				'error' => [
					'message' => 'This status does not exist',
					'href' => $this->root['router']->pathFor('commandesbyStatus',['status'=>$status])
				]
			];

			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

		try {
			$commandes_count = Commande::count();
		    $commandes_list = Commande::select('id','token','status','dateHeureRetrait','prixTot')->where('status','=',$status)->orderBy('dateHeureRetrait')->get();
		}
		catch (ModelNotFoundException $e){
			$error = [
				'error' => [
					'message' => 'Ressource not found',
					'href' => $this->root['router']->pathFor('commandesbyStatus',['status'=>$status])
				]
			];

			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

		$collection = [];

		foreach($commandes_list as $cat){
			$tab = $cat->toArray();
			array_push($collection, [
				'commande' => $tab,
				'links' => ['self' =>
					['href' => $this->root['router']->pathFor('commande',['id'=>$tab['id']])]]
				]);
		}

	    $commandes = array(
	        'nb' => $commandes_count,
	        'commandes' => $collection
	    );

	    $response->getBody()->write(json_encode($commandes));
		$response->withStatus(200);
	    return $response;
	}


//	Liste de toutes les commandes paginées
	public function listCommandesByPage($request, $response, $args) {
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		$offset = filter_var($args['offset'],FILTER_SANITIZE_NUMBER_INT);
		$limit = filter_var($args['limit'],FILTER_SANITIZE_NUMBER_INT);

		try {
			$commandes_count = Commande::count();
		}
		catch (ModelNotFoundException $e){
			$error = [
				'error' => [
					'message' => 'Ressource not found',
					'href' => $this->root['router']->pathFor('commandesbyPage',['offset'=>$offset,'limit'=>$limit])
				]
			];

			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

		$lastOffset = floor ($commandes_count / $limit) * $limit;

		if ($offset > $commandes_count || $offset < 0) {
			$error = [
				'error' => [
					'message' => 'Bad offset value',
					'href' => $this->root['router']->pathFor('commandesbyPage',['offset'=>$offset,'limit'=>$limit])
				]
			];

			$response = $response->withStatus(400);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

		if ($limit < 0) {
			$error = [
				'error' => [
					'message' => 'Bad limit value',
					'href' => $this->root['router']->pathFor('commandesbyPage',['offset'=>$offset,'limit'=>$limit])
				]
			];

			$response = $response->withStatus(400);
			$response->getBody()->write(json_encode($error));
			return $response;
		}


		try {
			$commandes_list = Commande::select('id','token','status','dateHeureRetrait','prixTot')->skip($offset)->take($limit)->orderBy('dateHeureRetrait')->orderBy('id')->get();
		}
		catch (ModelNotFoundException $e){
			$error = [
				'error' => [
					'message' => 'Ressource not found',
					'href' => $this->root['router']->pathFor('commandesbyPage',['offset'=>$offset,'limit'=>$limit])
				]
			];

			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

		$collection = [];

		foreach($commandes_list as $cat){
			$tab = $cat->toArray();
			array_push($collection, [
				'commande' => $tab,
				'links' => ['self' =>
					['href' => $this->root['router']->pathFor('commande',['id'=>$tab['id']])]]
					]);

		}

		if ($offset == 0){
			$prev = array(
				'href' => 	$this->root['router']->pathFor('commandesbyPage',['offset'=>$offset,'limit'=>$limit])
			);
		}
		elseif ($offset - $limit <0){
			$prev = array(
				'href' => 	$this->root['router']->pathFor('commandesbyPage',['offset'=>(0),'limit'=>$limit])
			);
		}
		else {
			$prev = array(
				'href' => 	$this->root['router']->pathFor('commandesbyPage',['offset'=>($offset-$limit),'limit'=>$limit])
			);
		}

		if ($offset*$limit >= $commandes_count){
			$next = array(
				'href' => 	$this->root['router']->pathFor('commandesbyPage',['offset'=>$offset,'limit'=>$limit])
			);
		}
		else {
			$next = array(
				'href' => 	$this->root['router']->pathFor('commandesbyPage',['offset'=>($offset+$limit),'limit'=>$limit])
			);
		}


		$links = array(
			'prev' => $prev,
			'next' => $next,
			'first' => ['href' => $this->root['router']->pathFor('commandesbyPage',['offset'=>0,'limit'=>$limit])],
			'last' => ['href' => $this->root['router']->pathFor('commandesbyPage',['offset'=>($lastOffset),'limit'=>$limit])]
		);

		$commandes = array(
			'nb' => $commandes_count,
			'commandes' => $collection,
			'links' => $links
		);

	    $response->getBody()->write(json_encode($commandes));
		$response->withStatus(200);
	    return $response;
	}

//	Données d'une commande
	public function seeCommande($request, $response, $args) {
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		$id = filter_var($args['id'],FILTER_SANITIZE_NUMBER_INT);

		try {
			$command = Commande::select('id','token','status','dateHeureRetrait','prixTot')->where('id','=',$id)->with('sandwiches', 'sandwiches.ingredients')->firstOrFail();
		}
		catch (ModelNotFoundException $e) {
			$error = [
				'error' => [
					'message' => 'Ressource not found',
					'href' => $this->root['router']->pathFor('commande',['id'=>$id])
				]
			];

			$response = $response->withStatus(404);
			$response->getBody()->write(json_encode($error));
			return $response;
		}

		$command = $command->toArray();
		if (count ($command['sandwiches']) > 0 && isset($command['sandwiches'])) {
			foreach ($command['sandwiches'] as $sandwichKey => $sandwich) {
				foreach ($sandwich['ingredients'] as $ingredientKey => $ingredient){
					unset ($command['sandwiches'][$sandwichKey]['ingredients'][$ingredientKey]['pivot']);
				}
			}
		}

		$commandes = array(
			'commande' => $command
		);

		$response->getBody()->write(json_encode($commandes));
		$response->withStatus(200);
	    return $response;
	}

	//	Données d'une commande
		public function changeCommandeStatus($request, $response, $args) {
			$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
			$id = filter_var($args['id'],FILTER_SANITIZE_NUMBER_INT);
			$data = $request->getParsedBody();
			if (isset($data['status'])) {
				$newStatus = filter_var($data['status'],FILTER_SANITIZE_NUMBER_INT);
			}
			else  {
				$error = [
					'error' => [
						'message' => 'No command status sent',
						'href' => $this->root['router']->pathFor('changeStatus',['id'=>$id])
					]
				];

				$response = $response->withStatus(400);
				$response->getBody()->write(json_encode($error));
				return $response;
			}

			try {
				$status = Commande::select('status')->where('id', '=', $id)->firstOrFail();
				$command = Commande::findOrFail($id);
			}
			catch (ModelNotFoundException $e) {
				$error = [
					'error' => [
						'message' => 'Ressource not found',
						'href' => $this->root['router']->pathFor('changeStatus',['id'=>$id])
					]
				];

				$response = $response->withStatus(404);
				$response->getBody()->write(json_encode($error));
				return $response;
			}

			try {
				$status_count = Status::count();
			}
			catch (ModelNotFoundException $e) {
				$error = [
					'error' => [
						'message' => 'Internal server error',
						'href' => $this->root['router']->pathFor('changeStatus',['id'=>$id])
					]
				];

				$response = $response->withStatus(500);
				$response->getBody()->write(json_encode($error));
				return $response;
			}

			$status = intval($status->toArray()['status']);

			if ($newStatus == $status+1 && $newStatus <= $status_count) {
				try {
					$command->status = $newStatus;
					$command->save();
				}
				catch (ModelNotFoundException $e) {
					$error = [
						'error' => [
							'message' => 'Bad request',
							'href' => $this->root['router']->pathFor('changeStatus',['id'=>$id])
						]
					];

					$response = $response->withStatus(400);
					$response->getBody()->write(json_encode($error));
					return $response;
				}
			}
			else {
				$error = [
					'error' => [
						'message' => 'Bad command status',
						'href' => $this->root['router']->pathFor('changeStatus',['id'=>$args['id']])
					]
				];

				$response = $response->withStatus(400);
				$response->getBody()->write(json_encode($error));
				return $response;
			}

			$response = $response->withStatus(200);
			return $response;
		}
}
