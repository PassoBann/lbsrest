<?php
namespace lbs\model;

class Commande extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'commande';
	protected $primaryKey = 'id';
	public $timestamps = false;

    public function sandwiches () {
		return $this->hasMany('lbs\model\Sandwich','id_commande');
	}

	public function get_status () {
		return $this->belongsTo('lbs\model\Status','status');
	}
}
