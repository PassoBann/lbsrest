<?php

/* board.html.twig */
class __TwigTemplate_b4624dab60741730a924ff5be48729adf11f0c730f8b2ec89fc0db2f2ac3d3b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1>Tableau de bord</h1>
<h2>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["ca"]) ? $context["ca"] : null), "html", null, true);
        echo "€ de chiffre d'affaire aujourd'hui</h2>
<h2>";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["cmd"]) ? $context["cmd"] : null), "html", null, true);
        echo " commande(s) aujourd'hui</h2>";
    }

    public function getTemplateName()
    {
        return "board.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 3,  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>Tableau de bord</h1>
<h2>{{ca}}€ de chiffre d'affaire aujourd'hui</h2>
<h2>{{cmd}} commande(s) aujourd'hui</h2>", "board.html.twig", "/var/www/REST/lbsrest/src/lbs/views/board.html.twig");
    }
}
