<?php

/* tailleList.html.twig */
class __TwigTemplate_96684ee65cc8cc64bee63adcc9fa3f5de189f795a3de93c86fbdca802f43c00b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"tailles\">
\t<h2>Liste des tailles de sandwich</h2>
\t";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tailles"]) ? $context["tailles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["taille"]) {
            // line 4
            echo "\t\t<form method=\"post\" action=\"";
            echo twig_escape_filter($this->env, (isset($context["path"]) ? $context["path"] : null), "html", null, true);
            echo "\">
\t\t\t<input type=\"hidden\" value=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($context["taille"], "id", array()), "html", null, true);
            echo "\" name=\"id\">
\t\t\t<input type=\"text\" value=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->getAttribute($context["taille"], "lib", array()), "html", null, true);
            echo "\" name=\"lib\">
\t\t\t<input type=\"text\" value=\"";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($context["taille"], "prix", array()), "html", null, true);
            echo "\" name=\"prix\">
\t\t\t<button type=\"submit\">Mettre à jour</button><br/>
\t\t</form>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['taille'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "tailleList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 11,  40 => 7,  36 => 6,  32 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"tailles\">
\t<h2>Liste des tailles de sandwich</h2>
\t{% for taille in tailles %}
\t\t<form method=\"post\" action=\"{{path}}\">
\t\t\t<input type=\"hidden\" value=\"{{taille.id}}\" name=\"id\">
\t\t\t<input type=\"text\" value=\"{{taille.lib}}\" name=\"lib\">
\t\t\t<input type=\"text\" value=\"{{taille.prix}}\" name=\"prix\">
\t\t\t<button type=\"submit\">Mettre à jour</button><br/>
\t\t</form>
\t{% endfor %}
</div>
", "tailleList.html.twig", "/var/www/projet-lbs/lbsrest/src/lbs/views/tailleList.html.twig");
    }
}
