<?php

/* ingredientDeleted.html.twig */
class __TwigTemplate_e35d9359b5c60c396b7546719c0bacdbc0e926e7ee8a1f43950947eab5af8aca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"message\">
\t\t<strong>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</strong>
</div>
";
    }

    public function getTemplateName()
    {
        return "ingredientDeleted.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"message\">
\t\t<strong>{{message}}</strong>
</div>
", "ingredientDeleted.html.twig", "/var/www/projet_lbs/lbsrest/src/lbs/views/ingredientDeleted.html.twig");
    }
}
