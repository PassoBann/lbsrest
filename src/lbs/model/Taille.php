<?php
namespace lbs\model;

class Taille extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'taille';
	protected $primaryKey = 'id';
	public $timestamps = false;

}
