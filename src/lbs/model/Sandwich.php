<?php
namespace lbs\model;

class Sandwich extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'sandwich';
	protected $primaryKey = 'id';
	public $timestamps = false;

  public function ingredients () {
		return $this->belongsToMany('lbs\model\Ingredient','sandwich_ingredient','id_sandwich','id_ingredient');
	}


	public function get_type() {
		return $this->belongsTo('lbs\model\TypePain','id_type_pain');
	}

	public function taille() {
		return $this->belongsTo('lbs\model\Taille','id_taille');
	}

	public function commande () {
		return $this->belongsTo('lbs\model\Commande','id_commande');
	}

}
