<?php
namespace lbs\api\controllers;
use lbs\model\User as User;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;


class UserController 
{
	protected $root;

	public function __construct($root)
	{
		$this->root = $root;
	}


	public function token(){

			$token = uniqid(rand(),true);
			$_SESSION['token'] = $token;

			return $token;

	}

	public function authView($request, $response) {

		$tok = $this->token();

		return $this->root['view']->render($response, 'login.html.twig',
			['token' => $tok]);


	}

	public function auth($request, $response) {

		$data = $request->getParsedBody();

		if(isset($data['username']) && isset($data['password']) && isset($data['token']) && $data['token'] == $_SESSION['token']) {


			$username = filter_var($data['username'], FILTER_SANITIZE_STRING);
			$password = filter_var($data['password'], FILTER_SANITIZE_STRING);

			try {
				$auth = User::where('user','=', $username)->firstOrFail();

				if(password_verify($data['password'],$auth->password)) {

					$title = "Vous êtes connecté";
					$message = "Félicitations ".$username. ", vous êtes connecté à l'interface de gestion";

					$_SESSION['is_connected'] = TRUE;
					$_SESSION['level'] = "administrator";


					return $this->root['view']->render($response, 'authSuccess.html.twig',
					['message' => $message,
					'title' => $title,
					'addIngredient' => $this->root['router']->pathFor('newIngredient'),
					'tdb' => $this->root['router']->pathFor('board'),
					'getIngredients' => $this->root['router']->pathFor('getIngredients'),
					'getTailleList' => $this->root['router']->pathFor('getTailles')]);

				}
				else {

					$message = "Mauvais mot de passe";
					$response = $response->withStatus(404);

					return $message;

				}

			}
			catch (ModelNotFoundException $e) {

				$response = $response->withStatus(404);
				$response = $response->getBody()->write('Aucun utilisateur trouvé pour cet identifiant.');

				return $response;

			}


		}
		else {

			$response = $response->withStatus(404);
			$message = "Une information du formulaire est manquante ou le Token a été modifié. Authentification annulée.";

			return $message;
		}

		$title = "Erreur d'authentification";
		return $this->root['view']->render($response, 'message.html.twig',
		['message' => $message,
		'title' => $title]);

	}
   
}
