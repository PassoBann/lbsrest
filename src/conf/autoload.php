<?php

function my_autoload ($class) {
    $link = 'src\\'.$class.'.php';
    $link = str_replace ('\\', DIRECTORY_SEPARATOR, $link);
    require_once ($link);
}
spl_autoload_register ('my_autoload');
