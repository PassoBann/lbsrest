<?php
namespace lbs\model;

class Status extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'status';
	protected $primaryKey = 'id';
	public $timestamps = false;

	
}