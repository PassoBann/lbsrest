<?php
require_once("../vendor/autoload.php");
header('Content-Type: application/json');

use Illuminate\Database\Capsule\Manager as Capsule;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use lbs\model\Categorie as Categorie;
use lbs\api\controllers\CatalogueController as CatalogueController;
use lbs\api\controllers\IngredientController as IngredientController;
use lbs\api\controllers\SandwichController as SandwichController;
use lbs\api\controllers\CommandeController as CommandeController;
use lbs\api\controllers\UserController as UserController;


$capsule = new Capsule;

$capsule->addConnection(parse_ini_file('../../LBSconfig/config.ini'),"default");
$capsule->setAsGlobal();
$capsule->bootEloquent();

$configuration = [
	'settings' => [
	'displayErrorDetails' => true,
	'production' => false,
	'tmpl_dir' => '../src/lbs/views'],
	'view' => function( $c ) {
		return new \Slim\Views\Twig(
			$c['settings']['tmpl_dir'],
			[
				'debug' => true,
				'cache' =>$c['settings']['tmpl_dir']
			]
 		);
 	}
];

$c = new \Slim\Container($configuration);

//END INIT

$app = new \Slim\App($c);
session_start();

$mw = function ($request, $response, $next) {

    if(isset($_SESSION['level']) && $_SESSION['level'] === "administrator" && isset($_SESSION['is_connected']) && $_SESSION['is_connected'] === TRUE) {

    		$response = $next($request, $response);

		}
	else {

			$response->getBody()->write("<h1>Problème d'authentification</h1>
				<p>Vous ne disposez pas des droits nécessaires pour accéder à cette page.</p>");

		}

    return $response;
};

//Lister les ingrédients disponibles par catégories
$app->get('/ingredients/', function (Request $request, Response $response) {

    return (new CatalogueController($this))->getIngredientList($request, $response);

})->setName('getIngredients')
->add($mw);


//Ajouter un ingrédient et sa catégorie
$app->get('/newingredient/', function (Request $request, Response $response) {

	return (new CatalogueController($this))->addIngredient($request, $response);

})->setName('newIngredient')
->add($mw);


$app->post('/newingredient/', function (Request $request, Response $response) {

	return (new IngredientController($this))->postIngredient($request, $response);

})->setName('newIngredient')
->add($mw);

//Supprimer les ingrédients sélectionnés
$app->post('/ingredients/delete/', function (Request $request, Response $response) {

    return (new IngredientController($this))->deleteIngredients($request, $response);

})->setName('deleteIngredients')
->add($mw);

//Lister les tailles de sandwiches
$app->get('/tailles/', function (Request $request, Response $response) {

    return (new SandwichController($this))->getTailleList($request, $response);

})->setName('getTailles')
->add($mw);

//Mettre à jour les tailles de sandwiches
$app->post('/taille/update/', function (Request $request, Response $response) {

    return (new SandwichController($this))->updateTaille($request, $response);

})->setName('updateTaille')
->add($mw);


$app->post('/login/', function (Request $request, Response $response) {

    return (new UserController($this))->auth($request, $response);

})->setName('login');

$app->get('/login/', function (Request $request, Response $response) {

    return (new UserController($this))->authView($request, $response);

})->setName('login');

//Retourne le tableau de bord
$app->get('/board/', function (Request $request, Response $response) {

	return (new CommandeController($this))->getBoard($request, $response);

})->setName('board')
->add($mw);


$app->run();
