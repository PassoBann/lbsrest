<?php

use lbs\utils\ConnectionFactory as ConnectionFactory;
use lbs\model\Categorie as Categorie;
use lbs\model\Ingredient as Ingredient;
require_once ('vendor/autoload.php');

header('Content-Type: application/json');

$db = new Capsule;

$db->addConnection(parse_ini_file('../LBS_config/config.ini'),"default");
$db->setAsGlobal();
$db->bootEloquent();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $name = filter_var($_POST['nom'], FILTER_SANITIZE_STRING);
    $desc = filter_var($_POST['description'], FILTER_SANITIZE_STRING);

    $newCat = new Categorie();
    $newCat->nom = $name;
    $newCat->description = $description;
    $newCat->save();

    http_response_code(201);
}

if (isset ($_GET['id'])) {
    $catById = Categorie::find($_GET['id']);
    echo json_encode($catById);
}
else {
    $cat = Categorie::select('id','nom')->get();
    $nb = Categorie::get()->count();
    $list = array(
        "nb" => $nb,
        "categories" => $cat
    );
    echo json_encode($list);
}
