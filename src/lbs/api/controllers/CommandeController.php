<?php
namespace lbs\api\controllers;

use lbs\model\Ingredient as Ingredient;
use lbs\model\Commande as Commande;
use lbs\model\Status as Status;
use lbs\model\Fidelity as Fidelity;
use lbs\model\Blacklist as BlackList;

use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
const CREATED 	= 1;
const PAYED 		=	2;
const PENDING 	= 3;
const READY 		= 4;
const DELIVERED = 5;


class CommandeController
{
	protected $root;


  public function __construct($root)

	{
		$this->root = $root;
	}

	public function createCommande($request, $response, $args) {


			$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
			$factory = new \RandomLib\Factory;
			$generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));
			$data = $request->getParsedBody();
			$retrait = $data["dateHeureRetrait"];
			$name = filter_var($data["consumerName"],FILTER_SANITIZE_STRING);

		if (isset($retrait) && isset($name)){
			$token = $generator->generateString(9);


			$newCom = new Commande;
			$newCom->dateHeureRetrait = $retrait ;
			$newCom->status = 1 ;
			$newCom->token = $token;
			$newCom->consumerName = $name;

			if ($newCom->save()) {

	            $response = $response->withStatus(201);
	            $id = $newCom->id;
	            $new = Commande::find($newCom->id);

	            unset($new['status']);
	            unset($new['datePayment']);
	            unset($new['tokenPayment']);

	            $newCom = array("commande" => $new);

		        $links = ['links' =>
			    [
			    	"PayCommande" => ['href' => $this->root['router']->pathFor('payCom',['id'=>$id])],
					"DetailsCommande" => ['href' => $this->root['router']->pathFor('detailCom',['id'=>$id])],
					"DisplayFacture" => ['href' => $this->root['router']->pathFor('displayFacture',['id'=>$id])],
					"UpdateCommande" => ['href' => $this->root['router']->pathFor('updateCommande',['id'=>$id])],
					"AddSandwich" => ['href' => $this->root['router']->pathFor('newSandwich',['id'=>$id])],
					"DeleteCommande" => ['href' => $this->root['router']->pathFor('deleteCommande',['id'=>$id])],
					"DisplayStatus" => ['href' => $this->root['router']->pathFor('statusCom',['id'=>$id])]

			    ]


			    ];

		        $combined = array_merge($newCom,$links);
		    	$response->getBody()->write(json_encode($combined));

	            $method = $request->getMethod();
	            $uri = $request->getUri();
	           	$response = $response->withHeader('Location:', $this->root['router']->pathFor('detailCom',['id'=>$id]));

	           	return $response ;

			} else {

					$response = $response->withStatus(500);
			        $response->getBody()->write(json_encode(["error" => "Database error"]));
			        return $response;
			}
		}

	}

	public function displayCommande($request, $response,$args) {
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		$id = $args['id'];
		$commande = Commande::find($id);
		if ($commande != NULL) {

			$good_token = $commande->token;

			if (isset($_GET['token']) && ($_GET['token'] == $good_token)) {



			    $response = $response->withStatus(200);
			    $tabStatus = Status::find($commande->status);
			    $commande['status'] = $tabStatus->lib;
			   	unset($commande['datePayment']);
	            unset($commande['tokenPayment']);
	            unset($commande['token']);
	            unset($commande['dateHeureSuppression']);
			    $commandeTab = array("commande" => $commande);

			    $links = ['links' =>
			        [

				    	"PayCommande" => ['href' => $this->root['router']->pathFor('payCom',['id'=>$id])],
				    	"DetailsCommande" => ['href' => $this->root['router']->pathFor('detailCom',['id'=>$id])],
					    "DisplayFacture" => ['href' => $this->root['router']->pathFor('displayFacture',['id'=>$id])],
					    "UpdateCommande" => ['href' => $this->root['router']->pathFor('updateCommande',['id'=>$id])],
					    "AddSandwich" => ['href' => $this->root['router']->pathFor('newSandwich',['id'=>$id])],
					    "DeleteCommande" => ['href' => $this->root['router']->pathFor('deleteCommande',['id'=>$id])]

			    	]

			    ];

			    $combined = array_merge($commandeTab,$links);
			   	$response->getBody()->write(json_encode($combined));
			   	return $response;


			} else {

					$response = $response->withStatus(403);
			        $response->getBody()->write(json_encode(["error" => "bad token"]));
			        return $response;
			}

	    } else {
			        $response = $response->withStatus(404);
			        $response->getBody()->write(
			        	json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('detailCom',['id'=>$id])]));


			    return $response;

	    }
	}

	public function payCommande($request, $response,$args) {
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		$data = $request->getParsedBody();
		$id = $args['id'];

		$factory = new \RandomLib\Factory;
		$generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));


		$commande = Commande::find($id);
		//verif id commande et status a 1
		if ($commande != NULL && $commande->status == 1 ) {

			$good_token = $commande->token;
			$idFidelity = "";
			$useReduct = 0 ; 

			//verif token

			if (isset($_GET['token']) && ($_GET['token'] == $good_token)) {
				//verif données
				if(isset($data["paymentCard"]) && isset($data["dateExp"]) && isset($data["crypto"])) {
					// si une carte de fidel est utilisée
					if(isset($data['fidelityToken']) ) {

						if (!isset($data['useReduct']) || $data['useReduct'] > 1 || $data['useReduct'] < 0) {
							$response = $response->withStatus(403);
			       			$response->getBody()->write(json_encode(["error" => "pliz enter if you want use or not your reduction (1 or 0"]));
			        		return $response;
						}

						$fidelityToken = $data['fidelityToken'] ;
						//verification si le token a deja été utilisé
						try {

							$isBlacklist = Blacklist::where("token","=",$fidelityToken)->firstOrFail();
							$response = $response->withStatus(403);
			       			$response->getBody()->write(json_encode(["error" => "reduction already used"]));
			        		return $response;

						} Catch (ModelNotFoundException $isBlacklist) {
							
						}

						$key = "key789789";
						$decoded = "";

						$decoded = \Firebase\JWT\JWT::decode($fidelityToken, $key, array('HS256'));
			
						$blacklist = New Blacklist ;
						$blacklist->token = $fidelityToken;
						$blacklist->save();
						$idFidelity = $decoded->id ;
						$useReduct = filter_var($data['useReduct'], FILTER_SANITIZE_NUMBER_INT);
						$fidelity = Fidelity::find($idFidelity);

						if($useReduct == 1) {
							//on utilise la reduction on remet donc a 0 le cumul et la reduction 
							$fidelity->montantCumul = 0 ;
							$fidelity->montantCumul = $fidelity->montantCumul + $commande->prixTot ;
							$fidelity->reduction = 0 ;
							$fidelity->save();
						} else {
							//on utilise pas la reduction nous mettons uniquement a jour le cumul
							$fidelity->montantCumul = $fidelity->montantCumul + $commande->prixTot ;
							$fidelity->save();
						}

					}

					$paymentCard = filter_var($data["paymentCard"], FILTER_SANITIZE_NUMBER_INT);
					$dateExp = $data["dateExp"];
					$crypto = filter_var($data["crypto"], FILTER_SANITIZE_NUMBER_INT);
					$commande->datePayment = date("Y-m-d H:i:s");
					$commande->tokenPayment = $generator->generateString(12);
					$commande->status = 2;


					//verif requete
					if ($commande->save()) {

						$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
						$response = $response->withStatus(200);
						unset($commande['status']);
						unset($commande['token']);
						unset($commande['dateHeureSuppression']);

						$commandeTab = array("paymentCommande" => $commande);
						$links = ['links' =>
			    			[

					    	"DetailsCommande" => ['href' => $this->root['router']->pathFor('detailCom',['id'=>$id])],
					    	"DisplayFacture" => ['href' => $this->root['router']->pathFor('displayFacture',['id'=>$id])],
					    	"UpdateCommande" => ['href' => $this->root['router']->pathFor('updateCommande',['id'=>$id])],
					    	"AddSandwich" => ['href' => $this->root['router']->pathFor('newSandwich',['id'=>$id])],
					    	"DeleteCommande" => ['href' => $this->root['router']->pathFor('deleteCommande',['id'=>$id])],
					    	"DisplayStatus" => ['href' => $this->root['router']->pathFor('statusCom',['id'=>$id])]

			    			]


			   			];

			    		$combined = array_merge($commandeTab,$links);
			   			$response->getBody()->write(json_encode($combined));
			   			return $response;

					} else {

						$response = $response->withStatus(500);
			        	$response->getBody()->write(json_encode(["error" => "Database error"]));
			        	return $response;
					}
					

				} else {
					$response = $response->withStatus(404);
			        $response->getBody()->write(json_encode(["error" => "missing parameter"]));
			        return $response;
				}

			}else {
					$response = $response->withStatus(403);
			        $response->getBody()->write(json_encode(["error" => "bad token"]));
			        return $response;
			}

		} else {

			$response = $response->withStatus(404);
			        $response->getBody()->write(
			        	json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('detailCom',['id'=>$id])]));
			return $response;

		}



	}

	public function detailsCommande($request, $response,$args) {
		$id = $args['id'];
		$commande = Commande::find($id);

		if ($commande != NULL ) {

			$good_token = $commande->token;
			//verif token
			if (isset($_GET['token']) && ($_GET['token'] == $good_token)) {

				$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
				$response = $response->withStatus(200);

				$commande = Commande::with("sandwiches","get_status","sandwiches.get_type","sandwiches.taille")->where("id","=", $id )->get();


					//$detailsTab = ["detailsCommande" => $commandetab] ;
					foreach ($commande as $com) {
						$sandwichs = [];

						foreach ($com->sandwiches as $sand) {
							$tab = [
								"id" => $sand->id ,
								"taille" => $sand->taille->lib ,
								"prix" => $sand->taille->prix ,
								"typePain" => $sand->get_type->lib ,
							];

							$sandwichs[] = $tab ;

						}

						$commandeTab = [ "commande" => [
							"id" => $com->id ,
							"dateHeureRetrait" => $com->dateHeureRetrait ,
		      				"consumerName" => $com->consumerName ,
		      				"prixTotal" => $com->prixTot ,
		      				"sandwichs" => $sandwichs
						]];


						$links = ['links' => [

					    	"PayCommande" => ['href' => $this->root['router']->pathFor('payCom',['id'=>$id])],
					    	"DisplayFacture" => ['href' => $this->root['router']->pathFor('displayFacture',['id'=>$id])],
					    	"UpdateCommande" => ['href' => $this->root['router']->pathFor('updateCommande',['id'=>$id])],
					    	"AddSandwich" => ['href' => $this->root['router']->pathFor('newSandwich',['id'=>$id])],
					    	"DeleteCommande" => ['href' => $this->root['router']->pathFor('deleteCommande',['id'=>$id])],
					    	"DisplayStatus" => ['href' => $this->root['router']->pathFor('statusCom',['id'=>$id])]
					    	]

			    		];

			    	$combined = array_merge($commandeTab,$links);

					$response->getBody()->write(json_encode($combined));
				   	return $response;

					}



			}else {
				$response = $response->withStatus(403);
			    $response->getBody()->write(json_encode(["error" => "bad token"]));
			    return $response;
			}


		} else {

			$response = $response->withStatus(404);
			        $response->getBody()->write(
			        	json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('detailCom',['id'=>$id])]));
			return $response;

		}

	}

	public function getFacture($request, $response,$args){
		$id = $args['id'];
		$commande = Commande::find($id);

		if ($commande != NULL && $commande->status >= 2 && $commande->status <= 5) {

			$good_token = $commande->token;
			//verif token
			if (isset($_GET['token']) && ($_GET['token'] == $good_token)) {

				$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
				$response = $response->withStatus(200);

				$commande = Commande::with("sandwiches","get_status","sandwiches.get_type","sandwiches.taille")->where("id","=", $id )->get();


					foreach ($commande as $com) {
						$sandwichs = [];

						foreach ($com->sandwiches as $sand) {
							$tab = [
								"id" => $sand->id ,
								"taille" => $sand->taille->lib ,
								"prix" => $sand->taille->prix ,
								"typePain" => $sand->get_type->lib ,
							];

							$sandwichs[] = $tab ;

						}

						$commandeTab = [ "facture" => [
							"tokenPayment" => $com->tokenPayment ,
							"consumerName" => $com->consumerName ,
		      				"datePayment" => $com->datePayment ,
		      				"prixTotal" => $com->prixTot ,
		      				"sandwichs" => $sandwichs
						]];

						$links = ['links' =>
			    			[

					    	"PayCommande" => ['href' => $this->root['router']->pathFor('payCom',['id'=>$id])],
					    	"DisplayFacture" => ['href' => $this->root['router']->pathFor('displayFacture',['id'=>$id])],
					    	"UpdateCommande" => ['href' => $this->root['router']->pathFor('updateCommande',['id'=>$id])],
					    	"AddSandwich" => ['href' => $this->root['router']->pathFor('newSandwich',['id'=>$id])],
					    	"DeleteCommande" => ['href' => $this->root['router']->pathFor('deleteCommande',['id'=>$id])],
					    	"DisplayStatus" => ['href' => $this->root['router']->pathFor('statusCom',['id'=>$id])]
			    			]


			    		];

			    	$combined = array_merge($commandeTab,$links);


					$response->getBody()->write(json_encode($combined));
				   	return $response;

					}

			}else {
				$response = $response->withStatus(403);
			    $response->getBody()->write(json_encode(["error" => "bad token"]));
			    return $response;
			}


		} else {

			$response = $response->withStatus(404);
			        $response->getBody()->write(
			        	json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('detailCom',['id'=>$id])]));
			return $response;

		}
	}

	public function deleteCommande($request, $response, $args){

		$id = $args['id'];

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

    	$data = $request->getParsedBody();

		//Vérification de l'existance de la ressource
		try {
			$c = Commande::findOrFail($id);
		} Catch (ModelNotFoundException $e) {
			$response = $response->withStatus(404);
			$response->getBody()->write(
			json_encode(["error" => "Ressource not found :".$this->root['router']->pathFor('deleted_command',['id'=>$id])]));
			return $response;
		}

		//Vérification de l'existance du token
		if (!isset($_GET['token'])){
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Token not found"]));
			return $response;
		}

		//Vérification du token
		$token = filter_var($_GET['token'], FILTER_SANITIZE_URL);
		if ($token != $c->token) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Bad token : ". $token]));
			return $response;
		}	

		//Vérification du statut de la commande
		if ($c->status != CREATED) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Command can't be deleted with status : ". Status::find($c->status)->lib]));
			return $response;
		}

		//Vérification de la date de suppression
		if ($c->dateHeureSuppression != NULL) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Command already deleted : ". Commande::find($id)->dateHeureSuppression]));
			return $response;
		}

		date_default_timezone_set('Europe/Paris');
		$datetime = new \DateTime();
		$c->dateHeureSuppression = $datetime->format("Y-m-d H:i:s");

		//Mise à jour dans la base de donnée
		try {
			$c->save();
		} Catch (\Exception $e) {
			$response = $response->withStatus(500);
			$response->getBody()->write(json_encode(["error" => "Query error : ". $e->getMessage()]));
			return $response;
		}

		$commande = [ "commande" => [
			"id" => $c->id ,
			"dateHeureRetrait" => $c->dateHeureRetrait,
			"status" => $c->get_status->lib ,
			"token" => $c->token ,
			"datePayment" => $c->datePayment ,
			"tokenPayment" => $c->tokenPayment ,
			"consumerName" => $c->consumerName ,
			"prixTotal" => $c->prixTot ,
			"dateHeureSuppression" => $c->dateHeureSuppression
		]];

		$links = ['links' =>
				[

				"PayCommande" => ['href' => $this->root['router']->pathFor('payCom',['id'=>$id])],
				"DisplayFacture" => ['href' => $this->root['router']->pathFor('displayFacture',['id'=>$id])],
				"UpdateCommande" => ['href' => $this->root['router']->pathFor('updateCommande',['id'=>$id])],
				"AddSandwich" => ['href' => $this->root['router']->pathFor('newSandwich',['id'=>$id])],
				"Self" => ['href' => $this->root['router']->pathFor('deleteCommande',['id'=>$id])],
				"DisplayStatus" => ['href' => $this->root['router']->pathFor('statusCom',['id'=>$id])]
				]
		];


		//Affichage
		$response = $response->withStatus(200);
		$combined = array_merge($commande, $links);
		$response->getBody()->write(json_encode($combined));

		return $response;
	}

public function updateCommande($request, $response, $args){

    $id = $args['id'];

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

    $data = $request->getParsedBody();

		//Vérification de l'existance de la ressource
		try {
			$c = Commande::findOrFail($id);
		} Catch (ModelNotFoundException $e) {
			$response = $response->withStatus(404);
			$response->getBody()->write(
			json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('updateCommande',['id'=>$id])]));
			return $response;
		}

		//Vérification de l'existance du token
		if (!isset($_GET['token'])){
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Token not found"]));
			return $response;
		}

		//Vérification du token
		$token = filter_var($_GET['token'], FILTER_SANITIZE_URL);
		if ($token != $c->token) {
			$response = $response->withStatus(403);
			$response->getBody()->write(json_encode(["error" => "Bad token : ". $token]));
			return $response;
		}

		//Vérification de la mise à jour de la date de retrait
    if (isset($data['dateheureretrait'])){
        $dateheureretrait = filter_var($data['dateheureretrait'],FILTER_SANITIZE_STRING);
        $c->dateHeureRetrait = $dateheureretrait;
    }

		//Mise à jour dans la base de donnée
		try {
			$c->save();
		} Catch (\Exception $e) {
			$response = $response->withStatus(500);
			$response->getBody()->write(json_encode(["error" => "Query error : ". $e->getMessage()]));
			return $response;
		}

		$commande = [ "commande" => [
			"id" => $c->id ,
			"dateHeureRetrait" => $c->dateHeureRetrait,
			"status" => $c->get_status->lib ,
			"token" => $c->token ,
					"datePayment" => $c->datePayment ,
					"tokenPayment" => $c->tokenPayment ,
					"consumerName" => $c->consumerName ,
					"prixTotal" => $c->prixTot ,
		]];

		$links = ['links' =>
				[

				"PayCommande" => ['href' => $this->root['router']->pathFor('payCom',['id'=>$id])],
				"DisplayFacture" => ['href' => $this->root['router']->pathFor('displayFacture',['id'=>$id])],
				"Self" => ['href' => $this->root['router']->pathFor('updateCommande',['id'=>$id])],
				"AddSandwich" => ['href' => $this->root['router']->pathFor('newSandwich',['id'=>$id])],
				"DeleteCommande" => ['href' => $this->root['router']->pathFor('deleteCommande',['id'=>$id])],
				"DisplayStatus" => ['href' => $this->root['router']->pathFor('statusCom',['id'=>$id])]
				]
		];


		//Affichage
		$response = $response->withStatus(200);
		$combined = array_merge($commande, $links);
		$response->getBody()->write(json_encode($combined));

		return $response;
  }


	public function createFidelity($request, $response) {

		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		$factory = new \RandomLib\Factory;
		$generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));
		$data = $request->getParsedBody();


		if (isset($data["password"]) && isset($data["name"]) && isset($data["reduction"])){


			$reduction = filter_var($data["reduction"],FILTER_SANITIZE_NUMBER_FLOAT);
			$card = $generator->generateString(6);
			$name = filter_var($data["name"],FILTER_SANITIZE_STRING);
			$password = password_hash($data["password"], PASSWORD_DEFAULT);
			
			$newFid = new Fidelity;
			$newFid->card = $card ;
			$newFid->reduction = $reduction;
			$newFid->name = $name;
			$newFid->password = $password;
			$newFid->montantCumul = 0 ;

			if ($newFid->save()) {
			
	            $response = $response->withStatus(201);
	            $id = $newFid->id;

	            unset($newFid['password']);
	            unset($newFid['id']);

	            $newFid = array("Fidelity" => $newFid);

		        $links = ['links' => 
			    [
			    	"PayCommande" => ['href' => $this->root['router']->pathFor('payCom',['id'=>$id])]
			    ]
			    ];

		        $combined = array_merge($newFid,$links);
		    	$response->getBody()->write(json_encode($combined));

	            $method = $request->getMethod();
	            $uri = $request->getUri();

	           	return $response ;

			} else {

					$response = $response->withStatus(500);
			        $response->getBody()->write(json_encode(["error" => "Database error"]));
			        return $response;
			}
		}else {
			echo "error";
		}

	}


	public function useFidelity($request, $response){
		$response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
		$card = $_SERVER['PHP_AUTH_USER'];
		$password = $_SERVER['PHP_AUTH_PW'];

		if ($card != "" && $password != "") {
			
			try {

				$fidelity = Fidelity::where("card","=",$card)->firstOrFail();

			} Catch (ModelNotFoundException $fidelity) {
				$response = $response->withStatus(404);
				$response->getBody()->write(
				json_encode(["error" => "Non-existent card"]));
				return $response;
			}

			$passCard = $fidelity->password;

			if(password_verify($password,$passCard)){
				$factory = new \RandomLib\Factory;
				$generator = $factory->getGenerator(new \SecurityLib\Strength(\SecurityLib\Strength::MEDIUM));
				$response = $response->withStatus(200);
				$key = "key789789";
				$jti = $generator->generateString(9);
    			$issuedAt = time();
				$token = array(
				    "card" => $fidelity->card,
				    "reduction" => $fidelity->reduction,
				    "id" => $fidelity->id,
				    "issuedAt" => $issuedAt,
					"jti" => $jti
				);

				$jwt = \Firebase\JWT\JWT::encode($token, $key);

				$use = [ "use" => [
					"montant" => $fidelity->montantCumul ,
					"reduction" => $fidelity->reduction,
					"tokenReduct" => $jwt,

				]];

				$response->getBody()->write(json_encode($use));

				return $response ;


			} else {

				$response = $response->withStatus(401);
				$response->getBody()->write(
				json_encode(["error" => "bad password"]));
				return $response;
			}
			

		} else {

			$response = $response->withStatus(401);
			$response->getBody()->write(json_encode(["error" => "empty user or password"]));
			return $response;

		}
	}

	//Retourne le nombre de commandes et le chiffre d'affaire de la journée
	public function getBoard($request, $response) {

		//Nombre de commandes aujourd'hui
		date_default_timezone_set('Europe/Paris');
		$datetime = new \DateTime();
		$today = $datetime->format("Y-m-d");

		$today = $today.'%';
		$c = Commande::select('prixTot');
		$c = $c->where('dateHeureRetrait', 'LIKE', $today)->get();

		$nbCommande = $c->count();

		//CA du jour
		$ca = 0;

		foreach ($c as $val) {
			$ca += $val->prixTot;
		}

		return $this->root['view']->render($response, 'board.html.twig',
			['ca' => $ca,
			'cmd' => $nbCommande]);

	}

}
