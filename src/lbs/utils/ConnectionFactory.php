<?php
namespace lbs\utils;
use Illuminate\Database\Capsule\Manager as DB;

class ConnectionFactory {

	static private $config,$db;

	public static function setConfig($nom_fichier) {
		self::$config = parse_ini_file($nom_fichier);	
	}



	public static function makeConnection() {
		if (!isset(self::$db)) {
			try
			{
				self::$db = new DB();
		        
		        self::$db->addConnection(self::$config);
		       // Make this Capsule instance available globally via static methods...
		        self::$db->setAsGlobal();
		        // Setup the Eloquent ORM... 
		        self::$db->bootEloquent();

				
			}
			catch(Exception $e)
			{
			        die('Erreur : '.$e->getMessage());
			}
	    }
	    return self::$db;
	}
}