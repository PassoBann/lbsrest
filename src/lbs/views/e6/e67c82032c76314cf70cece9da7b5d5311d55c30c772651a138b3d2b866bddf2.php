<?php

/* authFailed.html.twig */
class __TwigTemplate_c20f1948ee6b9e6185d1928b7fdd50f73a881dfb58257bd36288d5c99141a7d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1>Problème d'authentification</h1>
<p>Vous ne disposez pas des droits nécessaires pour accéder à cette page.</p>
<button><a href=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["link"]) ? $context["link"] : null), "html", null, true);
        echo "\">Connexion</a></button>";
    }

    public function getTemplateName()
    {
        return "authFailed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>Problème d'authentification</h1>
<p>Vous ne disposez pas des droits nécessaires pour accéder à cette page.</p>
<button><a href=\"{{link}}\">Connexion</a></button>", "authFailed.html.twig", "/var/www/REST/lbsrest/src/lbs/views/authFailed.html.twig");
    }
}
