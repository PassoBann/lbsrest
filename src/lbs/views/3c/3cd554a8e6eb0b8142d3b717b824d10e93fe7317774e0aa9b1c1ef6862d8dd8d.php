<?php

/* login.html.twig */
class __TwigTemplate_16ef6d3d9ee5aa7b575d505d478acf071a71edc17b942764dbe0b06c453282f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h2>Connexion</h2>
<form method=\"POST\" action=\"\">
\t<input type=\"text\" name=\"username\" placeholder=\"Username\">
\t<input type=\"password\" name=\"password\" placeholder=\"Password\">
\t<input type=\"text\" name=\"token\" style=\"display: none;\" value=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["token"]) ? $context["token"] : null), "html", null, true);
        echo "\">
\t<input type=\"submit\">
</form>";
    }

    public function getTemplateName()
    {
        return "login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 5,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h2>Connexion</h2>
<form method=\"POST\" action=\"\">
\t<input type=\"text\" name=\"username\" placeholder=\"Username\">
\t<input type=\"password\" name=\"password\" placeholder=\"Password\">
\t<input type=\"text\" name=\"token\" style=\"display: none;\" value=\"{{token}}\">
\t<input type=\"submit\">
</form>", "login.html.twig", "/var/www/projet_lbs/lbsrest/src/lbs/views/login.html.twig");
    }
}
