<?php
namespace lbs\model;

class TypePain extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'type_pain';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}