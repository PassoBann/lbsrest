<?php
namespace lbs\model;

class Blacklist extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'token_blacklist';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
}