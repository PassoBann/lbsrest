<?php
require_once("../vendor/autoload.php");
header('Content-Type: application/json');

// Token : page arche du module : ressources php : liste de librairies de qualité : randomLib
// Carte de fidélité : id, username, passwd, montant
// Authentification fidelite : HTTP BASIC + Json Web Token (PHP-JWT)

use Illuminate\Database\Capsule\Manager as Capsule;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use lbs\model\Categorie as Categorie;
use lbs\model\Ingredient as Ingredient;
use lbs\model\Commande as Commande;
use lbs\api\controllers\CatalogueController as CatalogueController;
use lbs\api\controllers\IngredientController as IngredientController;
use lbs\api\controllers\CommandeController as CommandeController;


use lbs\api\controllers\SandwichController as SandwichController;

$capsule = new Capsule;

$capsule->addConnection(parse_ini_file('../../LBSconfig/config.ini'),"default");
$capsule->setAsGlobal();
$capsule->bootEloquent();

$configuration = [
 'settings' => [
 'displayErrorDetails' => true,
'production' => false ]
];

$c = new \Slim\Container($configuration);

/*END INIT*/

//pour avoir en php les header authau il faut que apache lui envoie
// dans .htaccess il faut mettre commande : CGIPassAuth on 

//php-jwt
// identifier par header

$app = new \Slim\App($c);



//Recherche par ID
$app->get('/categories/{id}', function (Request $request, Response $response, $args) {

    return (new CatalogueController($this))->getCategory($request, $response, $args);

})->setName('category');

//Liste des catégories
$app->get('/categories/', function (Request $request, Response $response) {

    return (new CatalogueController($this))->listCategory($request, $response);

})->setName('list');

//Recherche d'ingrédient par ID
$app->get('/ingredients/{id}', function (Request $request, Response $response, $args) {

    return (new IngredientController($this))->getIngredient($request, $response, $args);

})->setName('ingredientid');

//Recherche des ingrédients d'une catégorie
$app->get('/categories/{id}/ingredients', function (Request $request, Response $response, $args) {

    return (new CatalogueController($this))->getIngredientCat($request, $response, $args);

})->setName('ingredientslist');


//Création d'une commande
$app->post('/commands/ajout', function (Request $request, Response $response, $args) {

    return (new CommandeController($this))->createCommande($request, $response, $args);

})->setName('newCom');

// afficher status commande
$app->get('/commands/{id}/status', function (Request $request, Response $response, $args) {

    return (new CommandeController($this))->displayCommande($request, $response, $args);

})->setName('statusCom');

//afficher la commande en details

$app->get('/commands/{id}/detailscommands', function (Request $request, Response $response, $args) {

    return (new CommandeController($this))->detailsCommande($request, $response, $args);

})->setName('detailCom');

// payer une commande

$app->post('/commands/{id}/pay', function (Request $request, Response $response, $args) {

    return (new CommandeController($this))->payCommande($request, $response, $args);

})->setName('payCom');

// facture
$app->get('/commands/{id}/facture', function (Request $request, Response $response, $args) {

    return (new CommandeController($this))->getFacture($request, $response, $args);

})->setName('displayFacture');

//Ajouter un Sandwich
$app->post('/commands/{id}/sandwich', function (Request $request, Response $response, $args) {

	return (new SandwichController($this))->addSandwich($request,$response, $args);

})->setname('newSandwich');


// Modifier la date de livraison d'une commande
$app->put('/commands/{id}/update', function (Request $request, Response $response, $args) {

  return (new CommandeController($this))->updateCommande($request, $response, $args);

})->setName('updateCommande');

// Recherche d'un sandwich par id
$app->get('/sandwich/{id}', function (Request $request, Response $response, $args) {

	return (new SandwichController($this))->getSandwich($request,$response, $args);

})->setname('sandwiches');


// json sandwiches : [ {
// sandwich  {id:12, ingredients[], size :3 type : 1 } 'links': 'self': {href:}]



// Modifier un sandwich en accord avec la commande
$app->put('/sandwich/{id}/update', function (Request $request, Response $response, $args) {

  return (new SandwichController($this))->updateSandwich($request, $response, $args);

})->setName('updateSandwich');


// Supprimer une commande qui n'a pas encore été payée
$app->delete('/commands/{id}/delete', function (Request $request, Response $response, $args) {

  return (new CommandeController($this))->deleteCommande($request, $response, $args);

})->setName('deleteCommande');


// Supprimer un sandwich d'une commande qui n'a pas encore été payée
$app->delete('/sandwich/{id}/delete', function (Request $request, Response $response, $args) {

  return (new SandwichController($this))->deleteSandwich($request, $response, $args);

})->setName('deleteSandwich');

//Ajouter une catégorie
$app->post('/categories/', function (Request $request, Response $response) {

    $response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

    $data = $request->getParsedBody();

    if (isset($data['nom']) && isset($data['description'])) {

        $nom = filter_var($data['nom'],FILTER_SANITIZE_STRING);
        $description = filter_var($data['description'],FILTER_SANITIZE_STRING);

        $new_category = new Categorie;
        $new_category->nom = $nom;
        $new_category->description = $description;
        $new_category->save();

        if ($new_category->save()) {
            $new_category = Categorie::find($new_category->id);
            $response = $response->withStatus(201);
            $response->getBody()->write(json_encode($new_category));

            //Ajout dans le header
            $method = $request->getMethod();
            $uri = $request->getUri();
            $response = $response->withHeader('Location:', $this['router']->pathFor('new_category',['id'=>$new_category->id]));
        }
        else {
            $response->getBody()->write("Une erreur est survenue");
        }

    }
    return $response;

})->setName('new_category');



//Créer carte de fidelité
$app->post('/fidelity/create', function (Request $request, Response $response) {

    return (new CommandeController($this))->createFidelity($request, $response);

})->setName('create_fidelity');

//utiliser carte de fidelité

$app->get('/fidelity/use', function (Request $request, Response $response) {

    return (new CommandeController($this))->useFidelity($request, $response);

})->setName('use_fidelity');



$app->run();
