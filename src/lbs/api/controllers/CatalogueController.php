<?php
namespace lbs\api\controllers;
use lbs\model\Categorie as Categorie;


class CatalogueController
{
	protected $root;
	public function __construct($root)
	{
		$this->root = $root;
	}

	public function getCategory($request, $response, $args) {

	    $id = $args['id'];

	    $response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

	    if (Categorie::find($id) != NULL) {
	    	$response = $response->withStatus(200);

	        $data = Categorie::find($id);
	        //$category = array(json_decode(Categorie::find($id)));
//

	        $array =
	        ['categorie' =>
	        	[
		        	'id' => $data->id,
		        	'nom' => $data->nom,
		        	'description' => $data->description,
	        	]
	        ];
	        $links =
	        ['links' =>
	        	[
	        		'ingredients' =>
	        			[
	        				'href' => $this->root['router']->pathFor('category',['id'=>$id]).'/ingredients'
	        			]
	        	]
	        ];

	        $combined = array_merge($array,$links);
	    	$response->getBody()->write(json_encode($combined));

	    }

	    else {
	        $response = $response->withStatus(404);
	        $response->getBody()->write(
	        	json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('category',['id'=>$id])]));
	    }

	    return $response;

	}

	public function listCategory($request, $response) {

		$categories_count = Categorie::count();
	    $categories_list = Categorie::select('id','nom','description')->get();

	    $collection = [];

		foreach($categories_list as $cat){

			array_push($collection, [
				'categorie' => $cat->toArray(),
				'links' => ['self' =>
					['href' => $this->root['router']->pathFor('category',['id'=>$cat->id])]]
					]);

		}

	    $categories = array(
	        'nb' => $categories_count,
	        'Categorie' => $collection
	    );

	    $response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');
	    $response->getBody()->write(json_encode($categories));

	    return $response;

	}


	//Retourne la liste des ingrédients d'une catégorie
	public function getIngredientCat($request, $response, $args) {

	    $id = $args['id'];

	    $response = $response->withHeader('Content-Type:', 'application/json;charset=utf8');

	    if (Categorie::find($id) != NULL) {

	    	$response = $response->withStatus(200);

				$cat = Categorie::find($id);
	        	$ingredients_list = $cat->has_ingredients()
	        	->where('cat_id','=',$id)
	        	->get();

	        $collection = [];

	        foreach($ingredients_list as $ing) {

	        	array_push($collection, [
	        		'ingredient' => $ing->toArray(),
	        		'links' => ['self' => ['href' => $this->root['router']->pathFor('ingredientid',['id'=>$ing->id])]]
	        		]);
	        }

	        $response->getBody()->write(json_encode($collection));

	    }

	    else {
	        $response = $response->withStatus(404);
	        $response->getBody()->write(
	        	json_encode(["error" => "ressource not found :".$this->root['router']->pathFor('ingredientslist',['id'=>$id])]));
	    }

	    return $response;

	}

	public function getIngredientList($request,$response) {


		$collection = [];

		$categorie = Categorie::select('id','nom')->get();
		foreach ($categorie as $key) {

			$ingredient = [];

			foreach ($key->has_ingredients->where('dateHeureSuppression', NULL) as $value) {
					$ingredient[] = array('id' => $value->id, 'nom' => $value->nom);
			}

			$tab = ['categorie' => $key->nom,
					'ingredient'=> $ingredient
			];
			$collection[] = $tab;
		}

		$path = $this->root['router']->pathFor('deleteIngredients');
		return $this->root['view']->render($response, 'ingredientList.html.twig',
			['collections' => $collection, 'path' => $path]);
	}


	public function addIngredient($request, $response) {

			$categorie = Categorie::select('nom','id')->get();
			$collection = [];

			foreach ($categorie as $value) {
				$tab = [
				'nom' => $value->nom,
				'id' => $value->id
				];

				$collection[] = $tab;
			}

		return $this->root['view']->render($response, 'addIngredient.html.twig',
			['collections' => $collection]);

	}

}
