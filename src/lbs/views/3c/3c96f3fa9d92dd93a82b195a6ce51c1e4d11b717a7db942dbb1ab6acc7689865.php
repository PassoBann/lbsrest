<?php

/* addIngredient.html.twig */
class __TwigTemplate_817c299b0bb0e4ed05922af8b719bbb2fa1520b28e1d18bb8169a93ca9fa9bac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1>Ajouter nouvel ingrédient</h1>
<form method=\"POST\" enctype=\"multipart/form-data\">
\t<select name=\"categorie\">
\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["collections"]) ? $context["collections"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["collection"]) {
            // line 5
            echo "\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["collection"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["collection"], "nom", array()), "html", null, true);
            echo "</option>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['collection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "\t</select>
\t<input type=\"text\" name=\"ingredient\" placeholder=\"Nom\" required>
\t<input type=\"text\" name=\"description\" placeholder=\"Description\" required>
\t<input type=\"text\" name=\"fournisseur\" placeholder=\"Fournisseur\" required>
\t<input type=\"file\" name=\"image\">
\t<input type=\"submit\">
</form>";
    }

    public function getTemplateName()
    {
        return "addIngredient.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 7,  28 => 5,  24 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>Ajouter nouvel ingrédient</h1>
<form method=\"POST\" enctype=\"multipart/form-data\">
\t<select name=\"categorie\">
\t{% for collection in collections %}
\t\t<option value=\"{{collection.id}}\">{{collection.nom}}</option>
\t{% endfor %}
\t</select>
\t<input type=\"text\" name=\"ingredient\" placeholder=\"Nom\" required>
\t<input type=\"text\" name=\"description\" placeholder=\"Description\" required>
\t<input type=\"text\" name=\"fournisseur\" placeholder=\"Fournisseur\" required>
\t<input type=\"file\" name=\"image\">
\t<input type=\"submit\">
</form>", "addIngredient.html.twig", "/var/www/projet_lbs/lbsrest/src/lbs/views/addIngredient.html.twig");
    }
}
