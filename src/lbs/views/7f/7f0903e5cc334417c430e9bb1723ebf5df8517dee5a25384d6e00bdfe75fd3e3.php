<?php

/* errors.html.twig */
class __TwigTemplate_9bb9d93c003ee26c87d43f7c2e65ef9c5737be3bf01325e692e1c369ff06c607 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1>ERREUR :</h1>
<p>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "errors.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>ERREUR :</h1>
<p>{{error}}</p>", "errors.html.twig", "/var/www/REST/lbsrest/src/lbs/views/errors.html.twig");
    }
}
