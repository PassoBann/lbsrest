<?php

/* ingredientList.html.twig */
class __TwigTemplate_ddbbbbd28a11db506af05007adaa6c179782c2b811a5efca265e9d49acc658bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"post\" action=\"";
        echo twig_escape_filter($this->env, (isset($context["path"]) ? $context["path"] : null), "html", null, true);
        echo "\">
<div class=\"ingredients_by_categorie\">
\t<h2>Liste des ingrédients par catégorie</h2>
\t";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["collections"]) ? $context["collections"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["collection"]) {
            // line 5
            echo "\t\t<strong>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["collection"], "categorie", array()), "html", null, true);
            echo " :</strong>
\t\t";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["collection"], "ingredient", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["collection"]) {
                // line 7
                echo "\t\t\t<input type=\"checkbox\" name=\"ingredients[]\" value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["collection"], "id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["collection"], "nom", array()), "html", null, true);
                echo "</input>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['collection'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "\t\t<br/>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['collection'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "</div>
 <button type=\"submit\">Supprimer</button>
</form>
";
    }

    public function getTemplateName()
    {
        return "ingredientList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 11,  50 => 9,  39 => 7,  35 => 6,  30 => 5,  26 => 4,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<form method=\"post\" action=\"{{path}}\">
<div class=\"ingredients_by_categorie\">
\t<h2>Liste des ingrédients par catégorie</h2>
\t{% for collection in collections %}
\t\t<strong>{{collection.categorie}} :</strong>
\t\t{% for collection in collection.ingredient %}
\t\t\t<input type=\"checkbox\" name=\"ingredients[]\" value=\"{{collection.id}}\">{{collection.nom}}</input>
\t\t{%endfor%}
\t\t<br/>
\t{% endfor %}
</div>
 <button type=\"submit\">Supprimer</button>
</form>
", "ingredientList.html.twig", "/var/www/projet-lbs/lbsrest/src/lbs/views/ingredientList.html.twig");
    }
}
