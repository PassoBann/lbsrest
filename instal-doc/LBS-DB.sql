-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `categorie` (`id`, `nom`, `description`) VALUES
(1,	'salade',	'Un peu de verdure'),
(2,	'crudités',	'5 fruits et légumes par jour'),
(3,	'charcuterie',	'Parce que c\'est bon'),
(4,	'viande',	'Des protéines'),
(5,	'fromage',	'3 produits laitiers par jour'),
(6,	'sauce',	'Parce que sans sauce c\'est un peu sec');

DROP TABLE IF EXISTS `commande`;
CREATE TABLE `commande` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `dateHeureRetrait` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `prixTot` int(11) DEFAULT NULL,
  `token` text NOT NULL,
  `datePayment` datetime NOT NULL,
  `tokenPayment` text NOT NULL,
  `consumerName` text NOT NULL,
  `dateHeureSuppression` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_status` (`status`),
  CONSTRAINT `fk_status` FOREIGN KEY (`status`) REFERENCES `status` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `commande` (`id`, `dateHeureRetrait`, `status`, `prixTot`, `token`, `datePayment`, `tokenPayment`, `consumerName`, `dateHeureSuppression`) VALUES
(1,	'2017-01-15 12:30:00',	5,	10,	'ghfjhjknjhffgkhlnjbhkghiljoknhjgrhtfygh',	'2017-01-15 09:30:00',	'jyugihjkqhuljuilhkjnkjgluhjbkhjyghbjhm',	'jordan',	'0000-00-00 00:00:00'),
(2,	'2017-03-15 12:30:00',	3,	12,	'sxcghdshgsdfkjgftuysethdyhdrthth',	'2017-01-17 12:30:00',	'sertyhrejhfgjyhdrtyuhrdthgdrht',	'loic',	'0000-00-00 00:00:00'),
(3,	'2017-02-15 12:30:00',	3,	9,	'yughewfuwesljfpiwajefehjsrgbwehfowe',	'2017-01-15 12:30:00',	'wuhetrfuwbeifoeshgawoiehfjwhbfiqwehgrfi',	'quentin',	'0000-00-00 00:00:00'),
(4,	'2017-02-02 09:11:58',	1,	12,	'wdgfnhwrqerytkruseyatsrtfyloiueya4wt3qWJYTHREGW',	'0000-00-00 00:00:00',	'',	'mickeal',	'0000-00-00 00:00:00'),
(5,	'2017-02-02 09:12:16',	2,	45,	'estjhewrrdyjyewarstyktjyerstfjydhgzsrd',	'2017-02-02 09:12:16',	'weutdyrywsertuhergdtygsexrdgwe',	'thibault',	'0000-00-00 00:00:00'),
(6,	'2017-02-02 09:12:32',	3,	32,	'wayethfdthdryhse4vtwe5vrtes5yw345rfwt',	'2017-02-02 09:12:32',	'erysebvtweftwe4twegrwefrwef',	'charle',	'0000-00-00 00:00:00'),
(7,	'2017-02-02 09:12:51',	4,	14,	'asgrjhseifhuesdygfhvuesisuhgvuisdrkg',	'2017-02-02 09:12:51',	'e5ytwqf3uhrtiwhuefiwhutgiuwheigr',	'kevin',	'0000-00-00 00:00:00'),
(8,	'2017-02-02 09:13:29',	5,	23,	'wrgesgerbyg6rgydgteryttyey',	'2017-02-02 09:13:29',	'we5yerureyfqwteythtfjyr',	'alexandre',	'0000-00-00 00:00:00'),
(9,	'2017-02-02 09:13:56',	2,	15,	'rgojwesirdgbfiwaesjhfovewjrnigjwersdg',	'2017-02-02 09:13:56',	'iwegfhbuiewsbfriqhwabeirbfhjwee',	'oliver',	'0000-00-00 00:00:00'),
(10,	'2017-02-02 09:16:00',	1,	21,	'wefjhbibhfwiesbhfiwjefijwend',	'0000-00-00 00:00:00',	'',	'olivier',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `fidelity`;
CREATE TABLE `fidelity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card` varchar(11) NOT NULL,
  `name` varchar(12) NOT NULL,
  `password` text NOT NULL,
  `montantCumul` varchar(12) NOT NULL,
  `reduction` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `fidelity` (`id`, `card`, `name`, `password`, `montantCumul`, `reduction`) VALUES
(6,	'b3jQXO',	'jordan',	'$2y$10$ydTmHdgiQApQPMSpQ0VuD.3ZxiTspNIEO9J8/AGZ/nTljfO1X1HrS',	'21',	'50'),
(7,	'HF9E6e',	'quentin',	'$2y$10$4auxZVJQ/kMRkXtfgH1Oe.ybmLs3/PMaayI5nL1a5/hEkcohYrUMW',	'0',	'50');

DROP TABLE IF EXISTS `ingredient`;
CREATE TABLE `ingredient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `description` text,
  `fournisseur` varchar(128) DEFAULT NULL,
  `img` varchar(128) DEFAULT NULL,
  `dateHeureSuppression` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ingredient` (`id`, `nom`, `cat_id`, `description`, `fournisseur`, `img`, `dateHeureSuppression`) VALUES
(1,	'laitue',	1,	'De la bonne laitue',	'lalaitue',	'imagelaitue',	NULL),
(2,	'roquette',	1,	'De la bonne roquette',	'laroquette',	'imageroquette',	NULL),
(3,	'mache',	1,	'De la bonne mache',	'lamache',	'imagemache',	NULL),
(4,	'carottes',	2,	'Des bonnes carottes rapées',	'lacarotte',	'imagecarotte',	NULL),
(5,	'concombre',	2,	'La fraicheur du concombre',	'leconcombre',	'imageconcombre',	NULL),
(6,	'tomate',	2,	'Un peu de couleur',	'latomate',	'imagetomate',	NULL),
(7,	'jambon',	3,	'Du bon jambon blanc',	'lejambon',	'imagejambon',	NULL),
(8,	'jambon cru',	3,	'Du bon jambon de Bayonne',	'bayonne',	'imagebayonne',	NULL),
(9,	'burger',	4,	'Le classique, le burger',	'leburger',	'imageburger',	NULL),
(10,	'confit',	4,	'Un bon confit de canard',	'leconfit',	'imageconfit',	NULL),
(11,	'emmental',	5,	'Emmental de 12 mois',	'lemmental',	'imageemmental',	NULL),
(12,	'comté',	5,	'Un comté affinage 18 mois',	'lecomté',	'imagecomté',	NULL),
(13,	'vinaigrette',	6,	'Notre vinaigrette maison',	'lavinaigrette',	'imagevinaigrette',	NULL),
(14,	'moutarde',	6,	'Moutarde en grains de Dijon',	'lamoutarde',	'imagemoutarde',	NULL);

DROP TABLE IF EXISTS `sandwich`;
CREATE TABLE `sandwich` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_pain` int(11) NOT NULL,
  `id_commande` int(11) DEFAULT NULL,
  `id_taille` int(11) DEFAULT NULL,
  `dateHeureSuppression` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_type_pain` (`id_type_pain`),
  KEY `fk_commande` (`id_commande`),
  CONSTRAINT `fk_commande` FOREIGN KEY (`id_commande`) REFERENCES `commande` (`id`),
  CONSTRAINT `fk_type_pain` FOREIGN KEY (`id_type_pain`) REFERENCES `type_pain` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sandwich` (`id`, `id_type_pain`, `id_commande`, `id_taille`, `dateHeureSuppression`) VALUES
(1,	1,	1,	1,	NULL),
(2,	3,	1,	1,	NULL),
(3,	2,	4,	4,	NULL),
(4,	1,	3,	3,	NULL);

DROP TABLE IF EXISTS `sandwich_ingredient`;
CREATE TABLE `sandwich_ingredient` (
  `id_ingredient` int(11) NOT NULL,
  `id_sandwich` int(11) NOT NULL,
  PRIMARY KEY (`id_ingredient`,`id_sandwich`),
  KEY `fk_id_sandwich` (`id_sandwich`),
  CONSTRAINT `fk_id_ingredient` FOREIGN KEY (`id_ingredient`) REFERENCES `ingredient` (`id`),
  CONSTRAINT `fk_id_sandwich` FOREIGN KEY (`id_sandwich`) REFERENCES `sandwich` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sandwich_ingredient` (`id_ingredient`, `id_sandwich`) VALUES
(1,	1),
(5,	1),
(11,	1),
(3,	2),
(12,	2),
(13,	2),
(1,	3),
(4,	3),
(6,	3),
(7,	3),
(8,	3),
(9,	3),
(10,	3),
(11,	3),
(12,	3),
(14,	3),
(1,	4),
(2,	4),
(3,	4),
(4,	4),
(5,	4),
(6,	4),
(7,	4);

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `lib` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `status` (`id`, `lib`) VALUES
(1,	'créée'),
(2,	'payée'),
(3,	'en cours'),
(4,	'prête'),
(5,	'livrée');

DROP TABLE IF EXISTS `taille`;
CREATE TABLE `taille` (
  `id` int(11) NOT NULL,
  `lib` text NOT NULL,
  `nbingr` int(11) NOT NULL,
  `prix` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `taille` (`id`, `lib`, `nbingr`, `prix`) VALUES
(1,	'petite faim',	3,	5),
(2,	'moyenne faim',	5,	7),
(3,	'grosse faim',	7,	9),
(4,	'ogre',	10,	12);

DROP TABLE IF EXISTS `token_blacklist`;
CREATE TABLE `token_blacklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `token_blacklist` (`id`, `token`) VALUES
(1,	'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjYXJkIjoiYjNqUVhPIiwicmVkdWN0aW9uIjoiNTAiLCJpZCI6NiwiaXNzdWVkQXQiOjE0ODc3ODIwNDcsImp0aSI6ImM0c0VJOEIrcCJ9.OcxGfx60oNiCV6ScxshjwyCwTGo8O0W0TQ52FNxwtvc');

DROP TABLE IF EXISTS `type_pain`;
CREATE TABLE `type_pain` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `lib` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `type_pain` (`id`, `lib`) VALUES
(1,	'blanc'),
(2,	'complet'),
(3,	'céréales');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `user`, `password`) VALUES
(1,	'Admin',	'$2y$10$QpHPP6z3kORBfDLCkIeCo.cHOGlxpu70DRbCmR/otudEPUXQnKitu');

-- 2017-02-22 21:15:02
